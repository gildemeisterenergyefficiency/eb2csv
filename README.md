# eb2csv

A Python (3.6) CLI tool for recording measurement data from Gildemeister Energy Box data loggers into csv files.

## About the Gildemeister Energy Box

The Gildemeister Energy Box is an IoT gateway that supports Modbus, OPC UA, Siemens S7 and other protocols.
Its primary use is to connect sites to the Gildemeister Energy Monitor.
See http://energy.gildemeister.com/en/save/energy-monitoring for details.  

## Installation

You can install eb2csv either via the Python Package Index (PyPI)
or from source.

To install using pip:

```bash
$ pip install eb2csv
```

**Source:** https://bitbucket.org/gildemeisterenergyefficiency/eb2csv/

## Requirements

eb2csv requires the following Python packages:

* click
* arrow
* requests

## License

eb2csv is released under the MIT license.
